#ifndef ASTAR_H
#define ASTAR_H

#include "Algorithm.h"
#include "Heuristic.h"
#include "Walker.h"
#include "Node.h"

class AStar : public Algorithm
{
    Q_OBJECT

public:
    explicit AStar(QObject *parent = nullptr);
    ~AStar() override;
    void runAlgorithm(Walker *walker) override;

private:
    Heuristic *heuristic;
    void reconstructPath(Walker *walker, Node *currentNode);
};

#endif // ASTAR_H
