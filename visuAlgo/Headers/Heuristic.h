#ifndef HEURISTIC_H
#define HEURISTIC_H

#include "Node.h"

class Node;

class Heuristic {
public:
    virtual double calculate(const Node& node1, const Node& node2) const = 0;
    virtual ~Heuristic() {}

};

class EuclideanDistance : public Heuristic {
public:
    double calculate(const Node& node1, const Node& node2) const override;
    ~EuclideanDistance() override {
    }

};

class ManhattanDistance : public Heuristic {
public:
    double calculate(const Node& node1, const Node& node2) const override;
    ~ManhattanDistance() override {
    }
};

#endif // HEURISTIC_H
