#ifndef WALKER_H
#define WALKER_H

#include <QObject>
#include <QVector>
#include <QElapsedTimer>
#include "Node.h"
#include "GridMatrix.h"
#include "Heuristic.h"
#include "Algorithm.h"

class Walker : public QObject
{
    Q_OBJECT

public:

    explicit Walker(Algorithm *algorithm,GridMatrix *gridMatrix,Heuristic *heuristic,const qint64 speed,QObject *parent = nullptr);

    ~Walker();
    void addVisitedNode(Node *node);
    void setPathNodes(const QVector<Node*> &pathNodes);
    void addNodeInPath(Node *node);
    void clearPath();

    Heuristic* getHeuristic() const;
    Node* getStartNode() const;
    Node* getEndNode() const;
    QElapsedTimer& getTimer() { return timer; }
    void setVisitedWalker(Node *node, bool visited);
    void reconstructPath(Walker* walker, Node* currentNode);
    QVector<Node *> getPathNodes() const;

    int getNumCols() const;
    int getNumRows() const;

    GridMatrix* getGridMatrix();
    int totalWeight(const QVector<Node*> &pathNodes);

    bool isPathFound;
    bool getIsPathFound();
    void pathNotFound();
    QVector<Node *> visitedNodes;

signals:
    void nodeVisited(Node *node);
    void nodeInPath(Node *node);
    void calculationFinished(double elapsedTime);
    void pathNotFoundSignal();
    void sendNodeWeight(int weight);

public slots:
    void calculatePath();

private:
    Algorithm *algorithm;
    GridMatrix *gridMatrix;
    Heuristic *heuristic;
    const qint64 speed;

    QVector<Node *> pathNodes;
    QElapsedTimer timer;
    int elapsedTime;
};


#endif // WALKER_H
