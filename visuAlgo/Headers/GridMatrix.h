#ifndef GRIDMATRIX_H
#define GRIDMATRIX_H

#include "Node.h"
#include "mazespec.h"

#include <QObject>
#include <QVector>
#include <QGraphicsScene>
#include <QRandomGenerator>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

class GridMatrix: public QGraphicsScene
{
                       Q_OBJECT
public:
    explicit GridMatrix(const mazeSpec& spec,QObject *parent = nullptr);

    ~GridMatrix();

    Node* getNode(int row, int col);

    void setStartNode(Node * node);
    void setEndNode(Node * node);
    Node* getStartNode() const;
    Node* getEndNode() const;

    void assignNeighbors();
    void generateMaze();
    const QVector<QVector<Node*>>& getMatrix() const;
    void clearObstacles();
    void clearStart();
    void clearEnd();
    void clearVisited();
    void clearVisitedExcept(const QVector<Node*>& nodesToKeep);
    void saveMatrixToJson(const QString& filePath);

    int getNumCols() const;
    int getNumRows() const;
    void clearWeight();
    QVector<Node*> getNodesWithWeight() const;
    void resetNodes();
    void setMatrix(const QVector<QVector<Node*>>& newMatrix);


public slots:
    void setOperationType(const QString &operation);

private:

    int rows;
    int cols;
    int cellSize;
    QVector<QPair<int, int>> directions;

    void initializeMatrix();
    QString operationType;

    QVector<QVector<Node*>> matrix;
};

#endif // GRIDMATRIX_H
