#ifndef BFS_H
#define BFS_H

#include "Algorithm.h"
#include "GridMatrix.h"
#include "Node.h"

#include "QStack"
#include "QSet"

class Walker;

class BFS : public Algorithm
{
    Q_OBJECT

public:
    BFS(QObject *parent = nullptr);
    ~BFS() override;
    void runAlgorithm(Walker * walker);

};

#endif // BFS_H
