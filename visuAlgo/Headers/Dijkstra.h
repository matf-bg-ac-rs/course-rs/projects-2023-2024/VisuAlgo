#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "Algorithm.h"
#include "GridMatrix.h"
#include "Node.h"

#include "QStack"
#include "QSet"

class Dijkstra : public Algorithm
{
    Q_OBJECT

public:
    Dijkstra(QObject *parent = nullptr);
    ~Dijkstra() override;
    void runAlgorithm(Walker * walker);
};

#endif // DIJKSTRA_H
