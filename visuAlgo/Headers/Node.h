#ifndef NODE_H
#define NODE_H

#include <QList>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QPair>
#include "Heuristic.h"

class Heuristic;

class Node: public QGraphicsRectItem
{
public:
    Node(int colScreenPos, int rowScreenPosition, int width, int height);

    Node(const Node& other);

    void setStartNode(const bool start);
    bool isStartNode() const;

    static void setExistsStart(bool exists);
    static void setExistsEnd(bool exists);

    void setEndNode(const bool end);
    bool isEndNode() const;

    void setVisited(const bool visited);
    bool isVisited() const;

    int getWeight() const;
    void setWeight(const int weight);

    void setObstacle(const bool obstacle);
    bool isObstacle() const;

    void addNeighbor(Node* neighbor);
    QPair<int, int> getPosition() const;

    const QList<Node*>& getNeighbors() const;
    double getFScore(Heuristic* heuristic, Node* goalNode) const;
    void setFScore(double fScore);
    int getGScore() const;
    void setGScore(int score);
    double getDistanceToNeighbor(const Node* neighbor) const;
    void setParent(Node* parentNode);
    Node* getParent() const;
    Node* startNode = nullptr;
    Node* endNode = nullptr;

    int getDistance();
    void setDistance(int d);
    bool isSetWeight() const;
    QColor gradientColor(int value, const QGradient& gradient);
    bool start = false;
    bool end = false;
    void resetNode();
    int getRow() const;
    int getCol() const;


private:
    QColor initicialColor;
    bool visited;
    bool obstacle;
    int weight = 0;
    Node* parent;
    QPair<int, int> position;
    static const int MAX_WEIGHT = 100;

    QList<Node*> neighbors;
    double fScore;
    QPointF lastMousePos;
    int distance;
    int gScore;

public slots:
    void onNodeInPath();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};

inline double getDistanceToNeighbor(const Node* node, const Node* neighbor)
{
    return node->getDistanceToNeighbor(neighbor);
}

#endif // NODE_H
