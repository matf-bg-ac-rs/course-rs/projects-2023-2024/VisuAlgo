#ifndef MAZESPEC_H
#define MAZESPEC_H

#include <QDialog>
#include <QMessageBox>
#include <QIntValidator>

namespace Ui {
class mazeSpec;
}

class mazeSpec : public QDialog
{
    Q_OBJECT

public:
    explicit mazeSpec(QWidget *parent = nullptr);
    ~mazeSpec();
    int getRows() const;
    int getColumns() const;
    int getCellSize() const;
    void setRows(int rows);
    void setCols(int cols);
private:
    int rows;
    int columns;
    int cellSize;
    short int directions;

    void setValidators();
    void clearLineEdits();
    void closeEvent(QCloseEvent *event) override;


signals:
    void dimensionsSelected(int rows, int cols);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::mazeSpec *ui;
};

#endif // MAZESPEC_H
