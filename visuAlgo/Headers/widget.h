#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "mazespec.h"
#include "GridMatrix.h"
#include "Walker.h"
#include "Heuristic.h"
#include "Algorithm.h"

#include<QGraphicsRectItem>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QString getAddRemove() const;
    QString getAlgorithm() const;
    QString getNodeWeight() const;
    int getWeight() const;

public slots:
    void showPathNotFound();

private slots:
    void on_pushButton_3_clicked();
    void drawMatrix();
    void onDialogAccepted();
    void clearMatrix();
    void comboboxItemChanged(int index);
    void handleComboBoxSelection(const QString &selectedItem);
    void onCalculateClicked();
    void algorithmSelected(int index);
    void heuristicSelected(int index);
    void weightTextChanged(const QString &text);
    void incrementLCDNumber();
    void incrementTimeElapsed(double timeElaspd);
    void onGenerateMazeClicked();
    void speedValueChanged(int value);
    void moveSpeedlabel();
    void onSaveMatrixButtonClicked();
    void onLoadButtonClicked();
    void loadMatrixFromJson(const QString& filePath);
    void incrementWeight(int value);



signals:
    void currentIndexChanged(const QString &selectedItemText);
    void operationTypeChanged(const QString &operationType);

private:
    Ui::Widget *ui;
    GridMatrix *gridMatrix;
    Algorithm *algorithm;
    Heuristic *heuristic;
    Walker * walker;
    mazeSpec *spec;
    QGraphicsScene *scene;
    QList<QGraphicsRectItem*> selectedItems;
    void clearPreviousPath();
};
#endif // WIDGET_H
