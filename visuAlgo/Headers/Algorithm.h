#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <QObject>

#include "Node.h"
class Walker;

class Algorithm : public QObject
{
    Q_OBJECT

public:

    explicit Algorithm(QObject *parent = nullptr);

    virtual void runAlgorithm(Walker *walker)=0;

private:
    Walker *walker;
};


#endif // ALGORITHM_H
