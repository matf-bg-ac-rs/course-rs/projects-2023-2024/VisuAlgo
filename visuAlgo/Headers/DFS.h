#ifndef DFS_H
#define DFS_H

#include "Algorithm.h"
#include "Walker.h"

class DFS : public Algorithm
{
    Q_OBJECT

public:
    explicit DFS(QObject *parent = nullptr);
     ~DFS() override;
    virtual void runAlgorithm(Walker *walker) override;

private:
    QList<Node *> reconstructPath(const QHash<Node *, Node *> &parentMap, Node *endNode);
};

#endif // DFS_H
