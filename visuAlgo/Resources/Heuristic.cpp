#include "../Headers/Heuristic.h"
#include <cmath>

double EuclideanDistance::calculate(const Node& node1, const Node& node2) const {
    return std::sqrt(std::pow(node2.getPosition().first - node1.getPosition().first, 2) + std::pow(node2.getPosition().second - node1.getPosition().second, 2));
}

double ManhattanDistance::calculate(const Node& node1,const Node& node2) const {
    return std::abs(node2.getPosition().first - node1.getPosition().first) + std::abs(node2.getPosition().second - node1.getPosition().second);
}
