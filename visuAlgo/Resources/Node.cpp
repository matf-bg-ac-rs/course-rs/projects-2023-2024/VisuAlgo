#include "../Headers/Node.h"
#include "../Headers/widget.h"
#include <QMouseEvent>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

extern bool existsStart = false;
extern bool existsEnd =  false;


Node::Node(int colScreenPos, int rowScreenPosition, int width, int height)
    : QGraphicsRectItem(colScreenPos, rowScreenPosition, width, height), visited(false), obstacle(false),start(false),end(false),weight(0), parent(nullptr),position(rowScreenPosition/width, colScreenPos/height)
{
    initicialColor = QColor(106,212,157);
    setBrush(initicialColor);
    setAcceptHoverEvents(true);
}

Node::Node(const Node& other)
    : QGraphicsRectItem(other.rect(), other.parentItem())
{
}


void Node::setExistsStart(bool exists){
    existsStart = exists;
}

void Node::setExistsEnd(bool exists){
    existsEnd = exists;
}

void Node::setStartNode(const bool start)
{
    this->start = start;
    if (start) {
        startNode = this;
        setBrush(Qt::yellow);
    } else {
        setBrush(initicialColor);
    }
}

bool Node::isStartNode() const
{
    return start;
}

void Node::setEndNode(const bool end)
{
    this->end = end;
    if (end) {
        endNode = this;
        setBrush(Qt::blue);
    } else {
        setBrush(initicialColor);
    }
}

bool Node::isEndNode() const
{
    return end;
}

void Node::setVisited(const bool visited)
{
    this->visited = visited;
    if (visited && !this->isStartNode() && !this->isEndNode()) {
        setBrush(QColor(255, 165, 0));

    } else if (this->isStartNode()) {
        setBrush(Qt::yellow);
    } else if (this->isEndNode()) {
        setBrush(Qt::blue);
    } else if (this->getWeight() > 0) {
        this->setWeight(this->getWeight());
    } else if (!visited){
        setBrush(initicialColor);
    }
}


bool Node::isVisited() const
{
    return visited;
}

int Node::getWeight() const
{
    return weight;
}

bool Node::isSetWeight() const
{
    if (weight == 0)
        return false;
    else
        return true;
}

void Node::setWeight(const int weight)
{
    this->weight = weight;

    if (weight > 0 && !this->obstacle && !this->isStartNode() && !this->isEndNode()) {
        setBrush(QColor(0, 255-2*weight, 0));
    }else if (this->obstacle) {
        setBrush(QColor(Qt::black));
    } else if (this->isStartNode()) {
        setBrush(Qt::yellow);
    } else if (this->isEndNode()) {
        setBrush(Qt::blue);
    } else if (weight == 0) {
        setBrush(initicialColor);
    }
}

QPair<int, int> Node::getPosition() const
{
    return position;
}

void Node::setObstacle(const bool obstacle)
{
    if (obstacle && !this->isStartNode() && !this->isEndNode()) {
        this->obstacle = obstacle;
        this->weight = 0;
        if(this->visited)
            this->setVisited(false);
        setBrush(Qt::black);
    } else if (this->isStartNode()) {
        setBrush(Qt::yellow);

    } else if (this->isEndNode()) {
        setBrush(Qt::blue);

    } else {
        this->obstacle = obstacle;
        setBrush(initicialColor);
    }
}


bool Node::isObstacle() const
{
    return obstacle;
}

void Node::addNeighbor(Node* neighbor)
{
    if(neighbor != nullptr)
        neighbors.append(neighbor);
}

const QList<Node*>& Node::getNeighbors() const
{
    return neighbors;
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Widget* widgetInstance = dynamic_cast<Widget*>(scene()->views().first()->parent());

    QString operationType = widgetInstance->getAddRemove();

    if (operationType == "Blocking" && event->button() == Qt::LeftButton
        && this->start == false && this->end == false) {
        setObstacle(!obstacle);
    } else if (operationType == "Begin Node" && event->button() == Qt::LeftButton) {
        if(this->start == true){
            setStartNode(!start);
            startNode = nullptr;
            setExistsStart(false);
        }
        else if(startNode == nullptr && existsStart == false){
            setStartNode(!start);
            startNode = this;
            setExistsStart(true);
        }

    } else if (operationType == "End Node" && event->button() == Qt::LeftButton) {
        if(this->end == true){
            setEndNode(!end);
            endNode = nullptr;
            setExistsEnd(false);
        }
        else if(endNode == nullptr && existsEnd == false){
            setEndNode(!end);
            endNode = this;
            setExistsEnd(true);
        }
    } else if (operationType == "Weight" && event->button() == Qt::LeftButton && this->obstacle == false) {
        int newWeight = widgetInstance->getWeight();
        setWeight(newWeight);
    }
    lastMousePos = event->scenePos();
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Widget* widgetInstance = dynamic_cast<Widget*>(scene()->views().first()->parent());

    QString operationType = widgetInstance->getAddRemove();

    if (operationType == "Blocking") {
        QPointF currentMousePos = event->scenePos();

        QRectF draggingArea(lastMousePos.x(), lastMousePos.y(), currentMousePos.x() - lastMousePos.x(), currentMousePos.y() - lastMousePos.y());

        QList<QGraphicsItem *> items = scene()->items(draggingArea);
        for (QGraphicsItem *item : items) {
            if (Node *node = qgraphicsitem_cast<Node *>(item)) {
                node->setObstacle(true);
            }
        }

        lastMousePos = currentMousePos;
    } else if (operationType == "Weight" && this->obstacle == false) {
        QPointF currentMousePos = event->scenePos();

        QRectF draggingArea(lastMousePos.x(), lastMousePos.y(), currentMousePos.x() - lastMousePos.x(), currentMousePos.y() - lastMousePos.y());

        QList<QGraphicsItem *> items = scene()->items(draggingArea);
        for (QGraphicsItem *item : items) {
            if (Node *node = qgraphicsitem_cast<Node *>(item)) {
                int newWeight = widgetInstance->getWeight();

                node->setWeight(newWeight);
            }
        }

        lastMousePos = currentMousePos;
    }
}

double Node::getFScore(Heuristic* heuristic, Node* goalNode) const
{
    double gScore = getGScore();
    double hScore = heuristic->calculate(*this, *goalNode);
    return gScore + hScore;
}

void Node::setFScore(double fScore)
{
    this->fScore = fScore;
}

int Node::getGScore() const
{
    return gScore;
}

void Node::setGScore(int score) {
    gScore = score;
}

double Node::getDistanceToNeighbor(const Node* neighbor) const
{
    if (!neighbor)
        return 0.0;

    int deltaX = neighbor->getPosition().first - position.first;
    int deltaY = neighbor->getPosition().second - position.second;

    return std::sqrt(deltaX * deltaX + deltaY * deltaY);
}

void Node::onNodeInPath() {
    if(!this->isEndNode() && !this->isStartNode()) {
        setBrush(QColor(Qt::red));
    } else if (this->isEndNode()) {
        setBrush(QColor(Qt::blue));
    } else if (this->isStartNode()) {
        setBrush(QColor(Qt::yellow));
    }
}

void Node::setParent(Node* parentNode)
{
    parent = parentNode;
}

Node* Node::getParent() const
{
    return parent;
}

int Node::getDistance()
{
    return distance;
}


void Node::setDistance(int d)
{
    distance = d;
}

void Node::resetNode()
{
    visited = false;
    obstacle = false;
    start = false;
    end = false;
    weight = 0;
    parent = nullptr;
    setBrush(initicialColor);
}


int Node::getRow() const {
    return position.first;
}


int Node::getCol() const {
    return position.second;
}
