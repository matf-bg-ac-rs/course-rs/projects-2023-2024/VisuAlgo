#include <QDebug>
#include <stack>
#include <QStack>
#include "../Headers/DFS.h"
#include "../Headers/Walker.h"

DFS::DFS(QObject *parent) : Algorithm(parent) {}

DFS::~DFS() {

}

void DFS::runAlgorithm(Walker *walker)
{
    walker->clearPath();
    walker->getGridMatrix()->clearVisited();

    Node *startNode = walker->getStartNode();
    Node *endNode = walker->getEndNode();

    if (!startNode || !endNode)
    {
        return;
    }

    QStack<Node *> stack;
    QSet<Node *> set;
    stack.push(startNode);
    startNode->setVisited(true);
    walker->addNodeInPath(startNode);

    while (!stack.isEmpty())
    {
        Node *currentNode = stack.pop();

        walker->setVisitedWalker(currentNode,true);

        if (currentNode == endNode)
        {
            walker->addVisitedNode(currentNode);
            walker->reconstructPath(walker,currentNode);
            walker->isPathFound = true;

            return;
        }
        walker->addVisitedNode(currentNode);

        const QList<Node *> &neighbors = currentNode->getNeighbors();
        for (Node *neighbor : neighbors)
        {
            if (!neighbor->isVisited() && !neighbor->isObstacle() && !set.contains(neighbor))
            {
                neighbor->setParent(currentNode);
                stack.push(neighbor);
                set.insert(neighbor);

            }
        }
    }

    if(!walker->isPathFound) {
        emit walker->pathNotFound();
    }
}
