#include "../Headers/widget.h"
#include "ui_widget.h"
#include "../Headers/mazespec.h"
#include "../Headers/GridMatrix.h"
#include "../Headers/Node.h"
#include "../Headers/Heuristic.h"
#include "../Headers/Algorithm.h"
#include "../Headers/BFS.h"
#include "../Headers/DFS.h"
#include "../Headers/AStar.h"
#include "../Headers/Dijkstra.h"
#include "limits"
#include <QFileDialog>
#include <QElapsedTimer>


#include "../Headers/Walker.h"
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QDebug>
#include <QComboBox>
#include <QObject>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget),
      spec(new mazeSpec(this)),
    gridMatrix(nullptr),walker(nullptr),algorithm(new AStar()),heuristic(new EuclideanDistance()),
    scene(new QGraphicsScene(this))
{
    ui->setupUi(this);

    ui->frame_12->hide();


    connect(spec, &mazeSpec::dimensionsSelected, this, &Widget::drawMatrix);
    connect(spec, &mazeSpec::accepted, this, &Widget::onDialogAccepted);
    connect(ui->pushButton_2, &QPushButton::clicked, this, &Widget::clearMatrix);
    connect(ui->PBCalculate, &QPushButton::clicked, this, &Widget::onCalculateClicked);
    connect(ui->PBGenerate, &QPushButton::clicked, this, &Widget::onGenerateMazeClicked);


    ui-> pushButton_2->setEnabled(false);



    QLinearGradient backgroundGradient;
    backgroundGradient.setStart(QPointF(0, 0));
    backgroundGradient.setFinalStop(QPointF(0, 1));
    backgroundGradient.setColorAt(0.0, QColor(85, 170, 127));
    backgroundGradient.setColorAt(1.0, QColor(13,5,69));
    backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);

    QBrush brush(backgroundGradient);

    QPalette palette;
    palette.setBrush(QPalette::Window, brush);
    setPalette(palette);

    connect(ui->CBAddRemove, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Widget::comboboxItemChanged);
    connect(this, &Widget::operationTypeChanged, this, &Widget::handleComboBoxSelection);
    connect(ui->CBAlgorithm, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Widget::algorithmSelected);
    connect(ui->CBHeuristic, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Widget::heuristicSelected);
    connect(ui->LEWeight, &QLineEdit::textChanged, this, &Widget::weightTextChanged);
    connect(ui->horizontalSlider, &QSlider::valueChanged, this,&Widget::moveSpeedlabel);
    connect(ui->PBSAVE, &QPushButton::clicked, this, &Widget::onSaveMatrixButtonClicked);
    connect(ui->PBLOAD, &QPushButton::clicked, this, &Widget::onLoadButtonClicked);


    ui->horizontalSlider->setValue(1);


    ui->labelSpeed->setText(QString::number(ui->horizontalSlider->value()));
    ui->horizontalSlider->setRange(1,100);
}

Widget::~Widget()
{
    if(gridMatrix)
        delete gridMatrix;
    if(walker)
        delete walker;
    if(algorithm)
        delete algorithm;
    if(heuristic)
        delete heuristic;
    delete ui;
}

void Widget::on_pushButton_3_clicked()
{
    spec->setModal(true);
    spec->exec();
}

void Widget::moveSpeedlabel(){
    QString currentValue = QString::number(ui->horizontalSlider->value());
    ui->labelSpeed->setText(currentValue);
}

void Widget::drawMatrix()
{
    ui-> pushButton_2->setEnabled(true);

    if (gridMatrix) {
        delete gridMatrix;
    }
    gridMatrix = new GridMatrix(*spec,this);
    ui->graphicsView->setScene(gridMatrix);
}

void Widget::onDialogAccepted() {
    spec->getRows();
    spec->getColumns();
    spec->getCellSize();
}

void Widget::onCalculateClicked() {

    if(!gridMatrix){
        return;
    }
    ui-> PBCalculate->setEnabled(false);
    ui-> pushButton_2->setEnabled(false);
    ui-> pushButton_3->setEnabled(false);
    ui-> PBGenerate->setEnabled(false);

    ui->frame_12->show();

    ui->lcdNumber->display(0);
    ui->lcdNumber_3->display(0);

    QString text = "Stats for " +
        ui->CBAlgorithm->currentText();
    ui->labelStats->setText(text);

    if(walker){
        delete walker;
        walker = nullptr;
    }
    const qint64 speed = ui->horizontalSlider->maximum() / ui->horizontalSlider->value();


    Node *start = gridMatrix->getStartNode();
    Node *end = gridMatrix->getEndNode();
    if (start == nullptr || end == nullptr) {
        QMessageBox::warning(this, "Warning", "Please select the start and end nodes.", QMessageBox::Ok);
    }
    walker = new Walker(algorithm,gridMatrix,heuristic,speed,this);
    connect(walker,&Walker::pathNotFoundSignal, this, &Widget::showPathNotFound);

    connect(walker,&Walker::nodeVisited,this,&Widget::incrementLCDNumber);
    connect(walker,&Walker::calculationFinished,this,&Widget::incrementTimeElapsed);
    connect(walker,&Walker::sendNodeWeight,this,&Widget::incrementWeight);


    if (walker) {
        walker->calculatePath();
    }

    ui-> PBCalculate->setEnabled(true);
    ui-> pushButton_2->setEnabled(true);
    ui-> pushButton_3->setEnabled(true);
    ui-> PBGenerate->setEnabled(true);
}

void Widget::clearMatrix()
{
    if (gridMatrix) {
        gridMatrix->resetNodes();
        Node::setExistsEnd(false);
        Node::setExistsStart(false);
        ui->lcdNumber->display(0);
        ui->lcdNumber_2->display(0);
        ui->lcdNumber_3->display(0);


        if (walker) {
            delete walker;
            walker = nullptr;
        }

        drawMatrix();
    }
}

QString Widget::getAddRemove() const{
    return ui->CBAddRemove->currentText();
}
QString Widget::getAlgorithm() const {
    return ui->CBAlgorithm->currentText();
}
QString Widget::getNodeWeight() const{
    return ui->CBAlgorithm->currentText();
}


void Widget::comboboxItemChanged(int index)
{
    QString selectedItemText = ui->CBAddRemove->itemText(index);

    emit currentIndexChanged(selectedItemText);
}

void Widget::handleComboBoxSelection(const QString &selectedItem)
{
    if (gridMatrix) {
        gridMatrix->setOperationType(selectedItem);
    }
}

void Widget::algorithmSelected(int index) {
    clearPreviousPath();

    if(algorithm){
        delete algorithm;
        algorithm = nullptr;
    }

    QString selectedAlgorithm = ui->CBAlgorithm->itemText(index);

    if (selectedAlgorithm == "BFS") {
        algorithm = new BFS();

    } else if (selectedAlgorithm == "DFS") {
        algorithm = new DFS();

    } else if (selectedAlgorithm == "A*") {
        algorithm = new AStar();


    } else if (selectedAlgorithm == "Dijkstra") {
        algorithm = new Dijkstra();
    }

}

void Widget::heuristicSelected(int index) {
    if(heuristic){
        delete heuristic;
        heuristic = nullptr;
    }
    QString selectedHeuristic = ui->CBHeuristic->itemText(index);

    if (selectedHeuristic == "Euclidean") {
        heuristic = new EuclideanDistance();

    } else if (selectedHeuristic == "Manhattan") {
        heuristic = new ManhattanDistance();

    }

}

int Widget::getWeight() const
{
    bool ok;
    int weight = ui->LEWeight->text().toInt(&ok);

    if (ok) {
        return weight;
    } else {
        return 0;
    }
}

void Widget::weightTextChanged(const QString &text)
{
    bool ok;
    text.toInt(&ok);

    if (!ok) {
        QMessageBox::warning(const_cast<QWidget*>(static_cast<const QWidget*>(this)), "Warning", "Weight must be a number.", QMessageBox::Ok);
    }}

void Widget::onGenerateMazeClicked() {
    if (gridMatrix) {
        gridMatrix->generateMaze();
    }
}

void Widget::incrementLCDNumber(){
    int currValue = ui->lcdNumber->value();
    ui->lcdNumber->display(currValue + 1);
}

void Widget::incrementTimeElapsed(double timeElapsed){
    timeElapsed = double    (timeElapsed);
    timeElapsed/=1000;
    ui->lcdNumber_2->display(timeElapsed);
}

void Widget::incrementWeight(int weight){
    int currValue = ui->lcdNumber_3->value();
    ui->lcdNumber_3->display(currValue + weight);
}

void Widget::speedValueChanged(int value) {
}

void Widget::clearPreviousPath()
{
    if (walker) {
        walker->clearPath();
        walker->isPathFound = false;

        if (gridMatrix) {
            QVector<Node*> nodesToKeep;
            Node* startNode = walker->getStartNode();
            Node* endNode = walker->getEndNode();

            if (startNode) nodesToKeep.append(startNode);
            if (endNode) nodesToKeep.append(endNode);

            gridMatrix->clearVisitedExcept(nodesToKeep);
        }
    }

    for (QGraphicsRectItem* item : selectedItems) {
        scene->removeItem(item);
        delete item;
    }
    selectedItems.clear();
}
void Widget::onSaveMatrixButtonClicked()
{
    if(!gridMatrix){
        QMessageBox::warning(this, "Warning", "Matrix is not created.", QMessageBox::Ok);
        return;
    }
    QString filePath = QFileDialog::getSaveFileName(this, "Sacuvaj Matricu", "", "JSON fajl (*.json)");

        if (!filePath.isEmpty()) {
        gridMatrix->saveMatrixToJson(filePath);
    }
}

void Widget::onLoadButtonClicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Ucitaj Matricu", "", "JSON fajl (*.json)");
        if (!filePath.isEmpty()) {
            loadMatrixFromJson(filePath);
        }
}

void Widget::loadMatrixFromJson(const QString& filePath)
{
    if (gridMatrix) {
        delete gridMatrix;
        gridMatrix = nullptr;
    }

    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QByteArray jsonData = file.readAll();
    file.close();

    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonData);
    QJsonObject matrixObject = jsonDocument.object();

    int loadedRows = matrixObject["rows"].toInt();
    int loadedColumns = matrixObject["columns"].toInt();
    qDebug() << loadedColumns << " " << loadedRows;
    spec->setRows(loadedRows);
    spec->setCols(loadedColumns);
    drawMatrix();
    QJsonArray nodesArray = matrixObject["nodes"].toArray();

    for (const QJsonValue& nodeValue : nodesArray) {
        QJsonObject nodeObject = nodeValue.toObject();
        int row = nodeObject["row"].toInt();
        int col = nodeObject["column"].toInt();
        Node* currentNode = gridMatrix->getNode(row,col);
        if (currentNode) {
            bool start = nodeObject["start"].toBool();
            bool end = nodeObject["end"].toBool();
            bool obstacle = nodeObject["obstacle"].toBool();
            int weight = nodeObject["weight"].toInt();

            if(start)
                currentNode->setStartNode(start);
            if(end)
                currentNode->setEndNode(end);
            if(obstacle)
                currentNode->setObstacle(obstacle);
            if(weight > 0)
                currentNode->setWeight(weight);

        }
    }

}
void Widget::showPathNotFound() {
    QMessageBox::information(this, "Path not found", "There is not path from start to end.");
}
