#include "../Headers/GridMatrix.h"
#include <QDebug>

GridMatrix::GridMatrix(const mazeSpec& spec,QObject *parent)
    : QGraphicsScene(parent), cellSize(spec.getCellSize()), operationType("Blocking")
{
    rows = spec.getRows();
    cols = spec.getColumns();

    directions = {
        { -1, 0 },
        { 1, 0 },
        { 0, -1 },
        { 0, 1 },
        { -1, -1 },
        { -1, 1 },
        { 1, -1 },
        { 1, 1 }
    };

    initializeMatrix();
    assignNeighbors();

}

const QVector<QVector<Node*>>& GridMatrix::getMatrix() const
{
    return matrix;
}

Node* GridMatrix::getNode(int row, int col)
{
    if (row >= 0 && row < rows && col >= 0 && col < cols) {
        return matrix[row][col];
    } else {
        return nullptr;
    }

}

Node* GridMatrix::getStartNode() const
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* currentNode = matrix[i][j];
            if (currentNode && currentNode->isStartNode()) {
                return currentNode;
            }
        }
    }
    return nullptr;
}

Node* GridMatrix::getEndNode() const
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* currentNode = matrix[i][j];
            if (currentNode && currentNode->isEndNode()) {
                return currentNode;
            }
        }
    }

    return nullptr;
}

QVector<Node*> GridMatrix::getNodesWithWeight() const
{
    QVector<Node*> nodesToKeep;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* currentNode = matrix[i][j];
            if (currentNode->getWeight() > 0 && !currentNode->isStartNode() && !currentNode->isEndNode())
                nodesToKeep.append(currentNode);
        }
    }

    return nodesToKeep;
}

int GridMatrix::getNumCols() const
{
    return cols;
}

int GridMatrix::getNumRows() const
{
    return rows;
}



void GridMatrix::initializeMatrix()
{
    matrix.resize(rows);
    for (int i = 0; i < rows; ++i) {
        matrix[i].resize(cols);
        for (int j = 0; j < cols; ++j) {
            matrix[i][j] = new Node(i*cellSize, j * cellSize, cellSize,cellSize);
            addItem(matrix[i][j]);
        }
    }
}

void GridMatrix::assignNeighbors()
{
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            Node *currentNode = matrix[i][j];

            if (currentNode)
            {
                for (const auto &direction : directions)
                {
                    int newRow = i + direction.first;
                    int newCol = j + direction.second;

                    if (newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols)
                    {
                        currentNode->addNeighbor(matrix[newRow][newCol]);
                    }
                }
            }
        }
    }

}

void GridMatrix::generateMaze(){
    this->clearObstacles();

    int nRows= this->getNumRows();
    int nColumns = this->getNumCols();

    int numberOfFields = nRows * nColumns;

    for(int i = 0; i < numberOfFields / 3; i++){
        int row = QRandomGenerator::global()->bounded(nRows);
        int col = QRandomGenerator::global()->bounded(nColumns);
        Node *currentNode = getNode(row,col);
        currentNode->setObstacle(true);

    }

}

GridMatrix::~GridMatrix()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            delete matrix[i][j];
        }
    }

    matrix.clear();


}
void GridMatrix::resetNodes()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node) {
                node->resetNode();
            }
        }
    }
}


void GridMatrix::clearObstacles()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node && node->isObstacle()) {
                node->setObstacle(false);
            }
        }
    }
}

void GridMatrix::clearStart()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node && node->isStartNode()) {
                node->setStartNode(false);
                node->startNode = nullptr;
            }
        }
    }
}

void GridMatrix::clearEnd()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node && node->isEndNode()) {
                node->setEndNode(false);
                node->endNode = nullptr;
            }
        }
    }
}

void GridMatrix::clearVisited()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node && node->isVisited()) {
                node->setVisited(false);
            }
        }
    }
}

void GridMatrix::clearVisitedExcept(const QVector<Node*>& nodesToKeep)
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            int weight = node->getWeight();
            if (node && node->isVisited() && !nodesToKeep.contains(node) && weight==0 && !node->isStartNode() && !node->isEndNode()) {
                node->setVisited(false);
            } else if (weight>0) {
                node->setWeight(node->getWeight());
            }
        }
    }
}

void GridMatrix::saveMatrixToJson(const QString& filePath)
{
    QJsonObject matrixObject;
    matrixObject["rows"] = rows;
    matrixObject["columns"] = cols;

    QJsonArray nodesArray;

    for (int row = 0; row < rows; ++row) {
        for (int col = 0; col < cols; ++col) {
            const Node* currentNode = matrix[row][col];
            if (currentNode) {
                QJsonObject nodeObject;
                nodeObject["row"] = row;
                nodeObject["column"] = col;
                nodeObject["start"] = currentNode->isStartNode();
                nodeObject["end"] = currentNode->isEndNode();
                nodeObject["obstacle"] = currentNode->isObstacle();
                nodeObject["weight"] = currentNode->getWeight();

                nodesArray.append(nodeObject);
            }
        }
    }

    matrixObject["nodes"] = nodesArray;

    QJsonDocument jsonDocument(matrixObject);

    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        out << jsonDocument.toJson();
        file.close();
    }
}


void GridMatrix::clearWeight()
{
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            Node* node = matrix[i][j];
            if (node && node->isSetWeight()) {
                node->setWeight(0);
            }
        }
    }
}

void GridMatrix::setOperationType(const QString &operation)
{
    operationType = operation;
}

void GridMatrix::setMatrix(const QVector<QVector<Node*>>& newMatrix)
{
    matrix = newMatrix;
}

