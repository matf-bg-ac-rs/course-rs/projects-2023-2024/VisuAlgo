#include "../Headers/Dijkstra.h"
#include "../Headers/Algorithm.h"
#include "../Headers/GridMatrix.h"
#include "../Headers/Node.h"
#include "limits"

#include "QQueue"
#include "QVector"
#include "../Headers/Walker.h"


Dijkstra::Dijkstra(QObject *parent)
    : Algorithm(parent)
{

}

Dijkstra::~Dijkstra() {

}


void Dijkstra::runAlgorithm(Walker *walker)
{
    walker->clearPath();
    walker->getGridMatrix()->clearVisited();


    Node *start = walker->getStartNode();
    Node *end = walker->getEndNode();

    if (!start || !end)
    {
        return;
    }


    QVector<Node*> unvisitedNodes;

    for(int i = 0; i < walker->getNumRows(); ++i) {
        for(int j = 0; j < walker->getNumCols(); ++j) {
            Node *node = walker->getGridMatrix()->getNode(i,j);
            if (node->isObstacle())
                continue;

            if (node == start)
                node->setDistance(0);
            else
                node->setDistance(INT_MAX);
            node->setParent(nullptr);
            unvisitedNodes.append(node);
        }
    }

    Node* currentNode = nullptr;

    while(!unvisitedNodes.isEmpty()) {
        int minDistance = INT_MAX;

        currentNode = nullptr;

        for(Node* node : unvisitedNodes) {
            if(node->getDistance() < minDistance) {
                minDistance = node->getDistance();
                currentNode = node;
            }
        }

        if(currentNode == nullptr) {
            walker->isPathFound = false;
            emit walker->pathNotFound();
            return;
        }

        unvisitedNodes.removeOne(currentNode);
        currentNode->setVisited(true);
        walker->addVisitedNode(currentNode);
        walker->setVisitedWalker(currentNode, true);


        for(Node* neighbor : currentNode->getNeighbors()) {
            if(unvisitedNodes.contains(neighbor) && !neighbor->isObstacle()) {
                int tentativeDistance = currentNode->getDistance() + neighbor->getWeight();

                if(tentativeDistance < neighbor->getDistance()) {
                    unvisitedNodes.removeOne(neighbor);
                    neighbor->setDistance(tentativeDistance);
                    neighbor->setParent(currentNode);
                    unvisitedNodes.append(neighbor);
                }
            }
        }


    }

    Node* current = end;

    walker->reconstructPath(walker,current);

    QVector<Node*> path = walker->getPathNodes();


    int i = path.size();
    if(i > 0 && path[i-1]->getParent() == start) {
        walker->isPathFound = true;
    }


    qDeleteAll(unvisitedNodes);
    unvisitedNodes.clear();
}

