#include "../Headers/Walker.h"
#include "../Headers/Node.h"
#include "../Headers/Heuristic.h"
#include "../Headers/Algorithm.h"

#include <QCoreApplication>
#include <QElapsedTimer>

Walker::Walker(Algorithm *algorithm,GridMatrix *gridMatrix,Heuristic *heuristic, const qint64 speed,QObject *parent)
    : QObject(parent),
    algorithm(algorithm),
    heuristic(heuristic),
    gridMatrix(gridMatrix),
    speed(speed)
{
}

Walker::~Walker()
{
    visitedNodes.clear();
    pathNodes.clear();
}


void Walker::addVisitedNode(Node *node)
{
    visitedNodes.append(node);
    emit nodeVisited(node);

    QElapsedTimer timer;
    timer.start();

    while (timer.elapsed() < speed) {
        QCoreApplication::processEvents();
    }
}

void Walker::setPathNodes(const QVector<Node *> &NewPath)
{
    pathNodes = NewPath;
}

QVector<Node *> Walker::getPathNodes() const
{
    return pathNodes;
}


void Walker::addNodeInPath(Node *node) {
    if(!node->isStartNode() && !node->isEndNode()) {
        pathNodes.append(node);
        node->onNodeInPath();
    }
}

void Walker::clearPath()
{
    pathNodes.clear();
    visitedNodes.clear();


}

void Walker::calculatePath() {

    timer.start();

    algorithm->runAlgorithm(this);
}

int Walker::totalWeight(const QVector<Node*> &path)
{
    int totalWeight = 0;
    for(int i = 0; i < path.size(); ++i) {
        Node* current = path[i];
        totalWeight += current->getWeight();
    }

    return totalWeight;
}

Node* Walker::getStartNode() const
{
    if (gridMatrix) {
        return gridMatrix->getStartNode();
    }
    else {
        return nullptr;
    }
}


Node* Walker::getEndNode() const
{
    if (gridMatrix)
        return gridMatrix->getEndNode();
    else
        return nullptr;
}



int Walker::getNumRows() const
{
    if (gridMatrix)
        return gridMatrix->getNumRows();
    else
        return -1;
}

int Walker::getNumCols() const
{
    if (gridMatrix)
        return gridMatrix->getNumCols();
    else
        return -1;
}

GridMatrix* Walker::getGridMatrix()
{
    return gridMatrix;
}

Heuristic* Walker::getHeuristic() const
{
    return heuristic;
}

void Walker::setVisitedWalker(Node *node, bool visited)
{
    if (node == nullptr) {

        return;
    }

    node->setVisited(visited);

    if (visited && !node->isStartNode() && !node->isEndNode()) {
        elapsedTime = timer.elapsed();
        emit calculationFinished(elapsedTime);

        node->setBrush(QColor(255, 165, 0));

    } else if (node->isStartNode()) {
        node->setBrush(Qt::yellow);
    } else if (node->isEndNode()) {
        node->setBrush(Qt::blue);
    }else {
        node->setBrush(QColor(106, 212, 157));
    }
}

void Walker::reconstructPath(Walker* walker,Node* currentNode){
    while (currentNode)
    {
        int nodeWeight = currentNode->getWeight();
        emit sendNodeWeight(nodeWeight);
        walker->addNodeInPath(currentNode);
        currentNode = currentNode->getParent();
    }

}

bool Walker::getIsPathFound() {
    return isPathFound;
}


void Walker::pathNotFound() {
    emit pathNotFoundSignal();
}
