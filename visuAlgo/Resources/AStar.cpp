#include "../Headers/AStar.h"

AStar::AStar(QObject *parent)
    : Algorithm(parent)
{
}
AStar::~AStar() {

}

void AStar::runAlgorithm(Walker *walker)
{
    walker->clearPath();
    walker->getGridMatrix()->clearVisited();

    heuristic = walker->getHeuristic();
    if (!walker || !walker->getStartNode() || !walker->getEndNode())
        return;

    Node *startNode = walker->getStartNode();
    Node *endNode = walker->getEndNode();

    QList<Node *> openSet;
    QList<Node *> closedSet;

    openSet.append(startNode);
    walker->addVisitedNode(startNode);

    while (!openSet.isEmpty())
    {
        Node *currentNode = openSet.first();
        for (Node *openNode : openSet)
        {
            if (openNode->getFScore(heuristic, endNode) < currentNode->getFScore(heuristic, endNode))
            {
                currentNode = openNode;
            }
        }

        openSet.removeOne(currentNode);
        closedSet.append(currentNode);

        if (currentNode == endNode)
        {
            walker->isPathFound = true;
            walker->reconstructPath(walker, currentNode);
            emit walker->calculationFinished(walker->getTimer().elapsed());
            return;
        }

        for (Node *neighbor : currentNode->getNeighbors())
        {
            if (neighbor->isObstacle() || closedSet.contains(neighbor))
                continue;

            int tentativeGScore = currentNode->getGScore() + currentNode->getDistanceToNeighbor(neighbor);
            tentativeGScore += neighbor->getWeight();

            if (!openSet.contains(neighbor) || tentativeGScore < neighbor->getGScore())
            {
                neighbor->setParent(currentNode);
                neighbor->setGScore(tentativeGScore);
                double hScore = heuristic->calculate(*neighbor, *endNode);
                neighbor->setFScore(tentativeGScore + hScore);

                if (!openSet.contains(neighbor))
                    openSet.append(neighbor);


                walker->addVisitedNode(neighbor);
                walker->setVisitedWalker(neighbor, true);
            }
        }
    }

    if (!walker->isPathFound)
    {
        emit walker->pathNotFound();
    }
}

void AStar::reconstructPath(Walker *walker, Node *currentNode)
{
    walker->clearPath();
    while (currentNode)
    {
        walker->addNodeInPath(currentNode);
        currentNode = currentNode->getParent();
    }
    walker->clearPath();
}
