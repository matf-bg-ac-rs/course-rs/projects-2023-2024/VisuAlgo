#include "../Headers/mazespec.h"
#include "ui_mazespec.h"


mazeSpec::mazeSpec(QWidget *parent) :
    QDialog(parent),
    rows(0),columns(0),
    cellSize(20),ui(new Ui::mazeSpec)
{
    ui->setupUi(this);

    QLinearGradient backgroundGradient;
    backgroundGradient.setStart(QPointF(0, 0));
    backgroundGradient.setFinalStop(QPointF(0, 1));
    backgroundGradient.setColorAt(0.0, QColor(69,184,255));
    backgroundGradient.setColorAt(1.0, QColor(8,0,158));
    backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);

    QBrush brush(backgroundGradient);

    QPalette palette;
    palette.setBrush(QPalette::Window, brush);
    setPalette(palette);
}

void mazeSpec::clearLineEdits()
{
    ui->rowsLineEdit->clear();
    ui->rowsLineEdit_2->clear();
}

mazeSpec::~mazeSpec()
{
    delete ui;
}

void mazeSpec::closeEvent(QCloseEvent *event) {
    clearLineEdits();
    QDialog::closeEvent(event);
}

void mazeSpec::setValidators()
{
    QIntValidator *validator = new QIntValidator(this);
    ui->rowsLineEdit->setValidator(validator);
    ui->rowsLineEdit_2->setValidator(validator);
}

void mazeSpec::on_buttonBox_accepted()
{

    setValidators();


    QString rowsText = ui->rowsLineEdit->text();
    QString columnsText = ui->rowsLineEdit_2->text();


    if (rowsText.isEmpty() || columnsText.isEmpty()) {
        QMessageBox::warning(this, "Warning", "Please fill in both fields.");
        return;
    }

    rows = rowsText.toInt();
    columns = columnsText.toInt();
    if (rows <=0 || columns <= 0) {
        QMessageBox::warning(this, "Upozorenje", "Broj vrsta i kolona mora biti pozitivan");
        return;
    }


    emit dimensionsSelected(rows, columns);

    clearLineEdits();
    accept();

}

int mazeSpec::getRows() const
{
    return rows;
}

int mazeSpec::getColumns() const
{
    return columns;
}

int mazeSpec::getCellSize() const{
    return cellSize;
}

void mazeSpec::on_buttonBox_rejected()
{
    clearLineEdits();
    close();
}

void mazeSpec::setRows(int rows){
    this->rows = rows;
}
void mazeSpec::setCols(int cols){
    this->columns = cols;
}
