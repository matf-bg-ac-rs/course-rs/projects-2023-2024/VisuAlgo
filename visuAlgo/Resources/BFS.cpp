#include "../Headers/BFS.h"
#include "../Headers/Algorithm.h"
#include "../Headers/GridMatrix.h"
#include "../Headers/Node.h"
#include "../Headers/Walker.h"
#include "QQueue"
#include "QVector"


BFS::BFS(QObject *parent)
    : Algorithm(parent)
{

}

BFS::~BFS() {
}

void BFS::runAlgorithm(Walker *walker)
{
    walker->clearPath();
    walker->getGridMatrix()->clearVisited();

    Node *start = walker->getStartNode();
    Node *end = walker->getEndNode();

    if (!start || !end)
    {
        return;
    }

    QQueue<Node*> queue;
    queue.enqueue(start);

    start->setVisited(true);
    walker->addVisitedNode(start);

    while (!queue.isEmpty()) {
        Node *currentNode = queue.dequeue();
        QList<Node*> neighbors = currentNode->getNeighbors();

        for (Node *neighbor : neighbors) {
            if (!neighbor->isVisited() && !neighbor->isObstacle()) {
                neighbor->setParent(currentNode);
                queue.enqueue(neighbor);
                walker->setVisitedWalker(neighbor, true);
                walker->addVisitedNode(neighbor);
                if(neighbor == end){
                    walker->reconstructPath(walker,currentNode);
                    walker->isPathFound = true;
                    return;
                }
            }
        }
    }

   walker->isPathFound = false;

    if(!walker->isPathFound) {
        emit walker->pathNotFound();
    }

}
