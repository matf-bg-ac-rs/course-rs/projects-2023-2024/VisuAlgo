QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Resources/AStar.cpp \
    Resources/Algorithm.cpp \
    Resources/BFS.cpp \
    Resources/DFS.cpp \
    Resources/Dijkstra.cpp \
    Resources/GridMatrix.cpp \
    Resources/Heuristic.cpp \
    Resources/Node.cpp \
    Resources/Walker.cpp \
    Resources/main.cpp \
    Resources/mazespec.cpp \
    Resources/widget.cpp

HEADERS += \
    Headers/AStar.h \
    Headers/Algorithm.h \
    Headers/BFS.h \
    Headers/DFS.h \
    Headers/Dijkstra.h \
    Headers/GridMatrix.h \
    Headers/Heuristic.h \
    Headers/Node.h \
    Headers/Walker.h \
    Headers/mazespec.h \
    Headers/widget.h

FORMS += \
    Forms/mazespec.ui \
    Forms/widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
