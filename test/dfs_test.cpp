#include <catch.hpp>

#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/mazespec.h"
#include "../visuAlgo/Headers/Walker.h"
#include "../visuAlgo/Headers/DFS.h"
#include "../visuAlgo/Headers/Heuristic.h"

TEST_CASE("DFS Algorithm Tests") {
    SECTION("For given begin and end node algorithm finds expected path between them") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);
        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node* start = matrix->getNode(0,0);
        Node* end = matrix->getNode(2,2);
        start->setStartNode(true);
        end->setEndNode(true);
        QVector<QPair<int,int>> expectedPath;
        expectedPath.push_back(std::make_pair(0,0));
        expectedPath.push_back(std::make_pair(1,1));
        expectedPath.push_back(std::make_pair(2,2));
        Walker* walker = new Walker(new DFS(),matrix,new EuclideanDistance(),0,nullptr);
        int i = 0;

        walker->calculatePath();
        auto path = walker->visitedNodes;


        for(auto node : path)
        {
            REQUIRE(node->getPosition().first == expectedPath[i].first);
            REQUIRE(node->getPosition().second == expectedPath[i].second);
            i++;
        }

        REQUIRE(end->isVisited());

    }
    SECTION("When there is no path from start to end node algorithm doesn't visit end node") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);
        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node* start = matrix->getNode(0,0);
        Node* end = matrix->getNode(2,2);
        start->setStartNode(true);
        end->setEndNode(true);
        matrix->getNode(1,0)->setObstacle(true);
        matrix->getNode(1,1)->setObstacle(true);
        matrix->getNode(0,1)->setObstacle(true);
        Walker* walker = new Walker(new DFS(),matrix,new EuclideanDistance(),0,nullptr);

        walker->calculatePath();

        REQUIRE_FALSE(end->isVisited());

    }
    SECTION("If begin node is same as end node algorithm finds path and doesn't visit any other node") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);
        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node* start = matrix->getNode(2,2);
        Node* end = matrix->getNode(2,2);
        start->setStartNode(true);
        end->setEndNode(true);
        Walker* walker = new Walker(new DFS(),matrix,new EuclideanDistance(),0,nullptr);

        walker->calculatePath();
        auto path = walker->visitedNodes;

        REQUIRE(end->isVisited());
        REQUIRE(path.size() == 1);

    }

    SECTION("If there are obstacles that are blocking direct path, algorithm still finds end node") {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);
        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node* start = matrix->getNode(0,0);
        Node* end = matrix->getNode(7,7);
        start->setStartNode(true);
        end->setEndNode(true);
        matrix->getNode(1,1)->setObstacle(true);
        matrix->getNode(2,2)->setObstacle(true);
        matrix->getNode(2,3)->setObstacle(true);
        matrix->getNode(3,2)->setObstacle(true);
        matrix->getNode(5,3)->setObstacle(true);
        matrix->getNode(6,7)->setObstacle(true);
        Walker* walker = new Walker(new DFS(),matrix,new EuclideanDistance(),0,nullptr);

        walker->calculatePath();

        REQUIRE(end->isVisited());
    }
}
