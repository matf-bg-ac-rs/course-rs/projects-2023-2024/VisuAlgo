#include "catch.hpp"

#include "../visuAlgo/Headers/AStar.h"
#include "../visuAlgo/Headers/Walker.h"
#include "../visuAlgo/Headers/Heuristic.h"
#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/mazespec.h"

#include "catch.hpp"
#include <list>
#include <utility>

TEST_CASE("AStar", "[class]") {

SECTION("proverava da li je putanja obiđenih čvorova ispravna na matrici 3x3") {

    mazeSpec spec;
    spec.setRows(3);
    spec.setCols(3);

    GridMatrix* matrix = new GridMatrix(spec, nullptr);
    Node *start = matrix->getNode(0, 0);
    Node *end = matrix->getNode(2, 2);

    std::list<std::pair<int, int>> expectedVisited;
    expectedVisited.push_back(std::make_pair(0, 0));
    expectedVisited.push_back(std::make_pair(0, 1));
    expectedVisited.push_back(std::make_pair(1, 0));
    expectedVisited.push_back(std::make_pair(1, 1));
    expectedVisited.push_back(std::make_pair(1, 2));
    expectedVisited.push_back(std::make_pair(2, 1));
    expectedVisited.push_back(std::make_pair(2, 0));
    expectedVisited.push_back(std::make_pair(0, 2));
    expectedVisited.push_back(std::make_pair(2, 2));

    start->setStartNode(true);
    end->setEndNode(true);

    Algorithm* algorithm = new AStar();
    Heuristic* heuristic = new EuclideanDistance();
    Walker* walker = new Walker(algorithm, matrix, heuristic, 1);
    walker->calculatePath();

    auto it = expectedVisited.begin();

    for (auto node : walker->visitedNodes) {
        REQUIRE(node->getPosition().first == it->first);
        REQUIRE(node->getPosition().second == it->second);
        ++it;
    }
  }

  SECTION("proverava da li je dostignut krajnji čvor") {

    mazeSpec spec;
    spec.setRows(3);
    spec.setCols(3);

    GridMatrix* matrix = new GridMatrix(spec, nullptr);
    Node* start = matrix->getNode(0, 0);
    Node* end = matrix->getNode(2, 2);
    start->setStartNode(true);
    end->setEndNode(true);

    Algorithm* algorithm = new AStar();
    Heuristic* heuristic = new EuclideanDistance();
    Walker* walker = new Walker(algorithm, matrix, heuristic, 1);
    walker->calculatePath();

    REQUIRE(end->isVisited());
  }

}

