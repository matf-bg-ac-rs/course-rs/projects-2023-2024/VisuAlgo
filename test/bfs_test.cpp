#include "catch.hpp"


#include<list>
#include<utility>

#include "../visuAlgo/Headers/BFS.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/Algorithm.h"
#include "../visuAlgo/Headers/mazespec.h"
#include "../visuAlgo/Headers/Walker.h"
#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/widget.h"

using namespace std;

TEST_CASE("BFS", "[class]"){

    SECTION("checks if the list of visited Nodes is correct on 3x3 matrix"){

        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node *start = matrix->getNode(0,0);
        Node *end = matrix->getNode(2,2);

        list<pair<int,int>> expectedVisited;
        expectedVisited.push_back(make_pair(0,0));
        expectedVisited.push_back(make_pair(0,1));
        expectedVisited.push_back(make_pair(1,0));
        expectedVisited.push_back(make_pair(1,1));
        expectedVisited.push_back(make_pair(0,2));
        expectedVisited.push_back(make_pair(1,2));
        expectedVisited.push_back(make_pair(2,0));
        expectedVisited.push_back(make_pair(2,1));
        expectedVisited.push_back(make_pair(2,2));


        start->setStartNode(true);
        end->setEndNode(true);

        Algorithm *algorithm = new BFS();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm,matrix,heuristic, 1);
        walker->calculatePath();

        for(auto node : walker->visitedNodes){
            REQUIRE(node->getPosition().first == expectedVisited.front().first);
            REQUIRE(node->getPosition().second == expectedVisited.front().second);
            expectedVisited.pop_front();
         }
    }

    SECTION("checks if the end node is visited"){

        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node *start = matrix->getNode(0,0);
        Node *end = matrix->getNode(2,2);
        start->setStartNode(true);
        end->setEndNode(true);

        Algorithm *algorithm = new BFS();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm,matrix,heuristic, 1);
        walker->calculatePath();

        REQUIRE(end->isVisited());

    }

    SECTION("When there is no path from start to end node algorithm doesn't visit end node") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);
        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node* start = matrix->getNode(0,0);
        Node* end = matrix->getNode(2,2);

        start->setStartNode(true);
        end->setEndNode(true);

        matrix->getNode(1,0)->setObstacle(true);
        matrix->getNode(1,1)->setObstacle(true);
        matrix->getNode(0,1)->setObstacle(true);
        Walker* walker = new Walker(new BFS(),matrix,new EuclideanDistance(),0,nullptr);

        walker->calculatePath();

        REQUIRE_FALSE(end->isVisited());

    }

}
