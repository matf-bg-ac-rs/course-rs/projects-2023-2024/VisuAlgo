#include <catch.hpp>

#include<list>
#include<utility>

#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/mazespec.h"

using namespace std;

TEST_CASE("GridMatrix", "[grid]")
{
    SECTION("GridMatrix::getNode - the node is in the range of matrix")
    {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int i = 2;
        int j = 2;

        Node * nodeTest = matrix->getMatrix()[i][j];
        Node * node = matrix->getNode(i,j);

        REQUIRE(node != nullptr);
        REQUIRE(node == nodeTest);

        delete matrix;
    }

    SECTION("GridMatrix::getNode - the node is not in the range of matrix")
    {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int i = 12;
        int j = 12;

        Node * node = matrix->getNode(i,j);

        REQUIRE(node == nullptr);

        delete matrix;
    }

    SECTION("GridMatrix::assignNeighbors")
    {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        Node * nodeUp = matrix->getNode(0,1);
        Node * nodeDown = matrix->getNode(2,1);
        Node * nodeLeft = matrix->getNode(1,0);
        Node * nodeRight = matrix->getNode(1,2);
        Node * nodeUpLeft = matrix->getNode(0,0);
        Node * nodeUpRight = matrix->getNode(0,2);
        Node * nodeDownLeft = matrix->getNode(2,0);
        Node * nodeDownRight = matrix->getNode(2,2);

        matrix->assignNeighbors();

        Node* currentNode = matrix->getNode(1, 1);
        const QList<Node*>& listOfNeighbors = currentNode->getNeighbors();

        REQUIRE(currentNode != nullptr);
        REQUIRE_FALSE(listOfNeighbors.empty());

        REQUIRE(listOfNeighbors[0] == nodeUp);
        REQUIRE(listOfNeighbors[1] == nodeDown);
        REQUIRE(listOfNeighbors[2] == nodeLeft);
        REQUIRE(listOfNeighbors[3] == nodeRight);
        REQUIRE(listOfNeighbors[4] == nodeUpLeft);
        REQUIRE(listOfNeighbors[5] == nodeUpRight);
        REQUIRE(listOfNeighbors[6] == nodeDownLeft);
        REQUIRE(listOfNeighbors[7] == nodeDownRight);

        delete matrix;

    }

    SECTION("GridMatrix::getStartNode - start node is set in the matrix")
    {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int startRow = 2;
        int startCol = 2;
        matrix->getNode(startRow, startCol)->setStartNode(true);


        Node* startNode = matrix->getStartNode();


        REQUIRE(startNode != nullptr);
        REQUIRE(startNode->isStartNode() == true);

        REQUIRE(startNode->getRow() == startRow);
        REQUIRE(startNode->getCol() == startCol);
    }

    SECTION("GridMatrix::getStartNode - start node is not set in the matrix")
    {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);


        Node* startNode = matrix->getStartNode();


        REQUIRE(startNode == nullptr);
    }

    SECTION("GridMatrix::getEndNode - end node is set in the matrix")
    {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int endRow = 5;
        int endCol = 5;
        matrix->getNode(endRow, endCol)->setEndNode(true);


        Node* endNode = matrix->getEndNode();


        REQUIRE(endNode != nullptr);
        REQUIRE(endNode->isEndNode() == true);

        REQUIRE(endNode->getRow() == endRow);
        REQUIRE(endNode->getCol() == endCol);
    }

    SECTION("GridMatrix::getEndNode - end node is not set in the matrix")
    {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);


        Node* endNode = matrix->getEndNode();


        REQUIRE(endNode == nullptr);
    }
    SECTION("GridMatrix::clearObstacles - for given matrix with obstacles check if every obstacle is cleared")
    {
        mazeSpec spec;
        spec.setRows(5);
        spec.setCols(5);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        matrix->getNode(1,0)->setObstacle(true);
        matrix->getNode(3,0)->setObstacle(true);
        matrix->getNode(1,3)->setObstacle(true);
        matrix->getNode(2,4)->setObstacle(true);
        matrix->getNode(3,2)->setObstacle(true);
        matrix->getNode(2,1)->setObstacle(true);
        matrix->getNode(4,1)->setObstacle(true);

        matrix->clearObstacles();

        for (int i = 0; i < matrix->getNumCols(); i++){
            for(int j = 0; j < matrix->getNumCols(); j++){
                REQUIRE(!matrix->getNode(i,j)->isObstacle());
            }
        }
    }
    SECTION("GridMatrix::clearWeight - for given matrix with weights check if total weight is 0")
    {
        mazeSpec spec;
        spec.setRows(5);
        spec.setCols(5);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        int totalWeight = 0;

        matrix->getNode(1,0)->setWeight(20);
        matrix->getNode(3,0)->setWeight(1);
        matrix->getNode(1,3)->setWeight(92);
        matrix->getNode(2,4)->setWeight(32);
        matrix->getNode(3,2)->setWeight(14);
        matrix->getNode(2,1)->setWeight(33);


        matrix->clearWeight();

        for (int i = 0; i < matrix->getNumCols(); i++){
            for(int j = 0; j < matrix->getNumCols(); j++){
                totalWeight += matrix->getNode(i,j)->getWeight();
            }
        }

        REQUIRE(totalWeight == 0);
    }

    SECTION("GridMatrix::clearStart() - this method sets value of startNode to nullptr "
            "and colors start node in initicialColor")
    {
        mazeSpec spec;
        spec.setRows(5);
        spec.setCols(5);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int startRow = 1;
        int startCol = 1;
        matrix->getNode(startRow, startCol)->setStartNode(true);

        matrix->clearStart();

        Node* startNode = matrix->getStartNode();

        REQUIRE(startNode == nullptr);
    }

    SECTION("GridMatrix::clearEnd() - this method sets value of endNode to nullptr "
            "and colors start node in initicialColor")
    {
        mazeSpec spec;
        spec.setRows(5);
        spec.setCols(5);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int startRow = 1;
        int startCol = 1;
        matrix->getNode(startRow, startCol)->setEndNode(true);

        matrix->clearEnd();

        Node* endNode = matrix->getEndNode();

        REQUIRE(endNode == nullptr);
    }

    SECTION("GridMatrix::clearVisited() - this method sets value visited of all visited nodes to false")
    {
        mazeSpec spec;
        int rows = 3;
        int cols = 3;
        spec.setRows(rows);
        spec.setCols(cols);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        int row = 0;
        int col = 0;
        matrix->getNode(row, col)->setVisited(true);

        row = 1;
        col = 1;
        matrix->getNode(row, col)->setVisited(true);

        row = 2;
        col = 2;
        matrix->getNode(row, col)->setVisited(true);

        matrix->clearVisited();

        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                bool result = matrix->getNode(i,j)->isVisited();
                REQUIRE(result == false);
            }
        }
    }


    SECTION("GridMatrix::resetNodes() - this method for all nodes that aren't nullptr"
            "sets: visited = false, obstacle = false, start = false, end = false, weight = 0, parent = nullptr")
    {
        mazeSpec spec;
        int rows = 3;
        int cols = 3;
        spec.setRows(rows);
        spec.setCols(cols);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        Node* node = matrix->getNode(0,0);

        int row = 0;
        int col = 0;
        matrix->getNode(row, col)->setVisited(true);
        matrix->getNode(row, col)->setObstacle(true);

        row = 1;
        col = 1;
        matrix->getNode(row, col)->setVisited(true);
        matrix->getNode(row, col)->setWeight(5);
        matrix->getNode(row,col)->setStartNode(true);

        row = 2;
        col = 2;
        matrix->getNode(row, col)->setVisited(true);
        matrix->getNode(row, col)->setEndNode(true);
        matrix->getNode(row, col)->setWeight(3);
        matrix->getNode(row, col)->setParent(node);

        matrix->resetNodes();

        for(int i = 0; i < 3; i++) {

            bool resultVisited = matrix->getNode(i,i)->isVisited();
            bool resultStartNode = matrix->getNode(i,i)->isStartNode();
            bool resultEndNode = matrix->getNode(i,i)->isEndNode();
            int resultWeight = matrix->getNode(i,i)->getWeight();
            bool resultObstacle = matrix->getNode(i,i)->isObstacle();
            Node* resultParent = matrix->getNode(i,i)->getParent();

            REQUIRE(resultVisited == false);
            REQUIRE(resultStartNode == false);
            REQUIRE(resultEndNode == false);
            REQUIRE(resultObstacle == false);
            REQUIRE(resultWeight == 0);
            REQUIRE(resultParent == nullptr);
        }
    }

    SECTION("GridMatrix::getNodesWithWeight() - this method returns vector which contains all nodes"
            "which weight is > 0 and are not start and end node")
    {
        mazeSpec spec;
        int rows = 3;
        int cols = 3;
        spec.setRows(rows);
        spec.setCols(cols);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);

        matrix->getNode(0,0)->setStartNode(true);
        matrix->getNode(0,0)->setWeight(0);
        matrix->getNode(0,1)->setWeight(1);
        matrix->getNode(0,2)->setWeight(2);
        matrix->getNode(1,0)->setWeight(0);
        matrix->getNode(1,1)->setWeight(0);
        matrix->getNode(1,2)->setWeight(0);
        matrix->getNode(2,0)->setWeight(0);
        matrix->getNode(2,1)->setWeight(3);
        matrix->getNode(2,2)->setWeight(3);
        matrix->getNode(2,2)->setEndNode(true);


        list<pair<int,int>> accurateResult;
        accurateResult.push_back(make_pair(0,1));
        accurateResult.push_back(make_pair(0,2));
        accurateResult.push_back(make_pair(2,1));



        for(auto node : matrix->getNodesWithWeight()){
            REQUIRE(node->getPosition().first == accurateResult.front().second);
            REQUIRE(node->getPosition().second == accurateResult.front().first);
            accurateResult.pop_front();
        }

    }

    SECTION("GridMatrix::clearVisitedExcept: this method clears visited nodes except specified nodes") {
        mazeSpec spec;
        int rows = 3;
        int cols = 3;
        spec.setRows(rows);
        spec.setCols(cols);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);

        Node* keepNode1 = gridMatrix->getNode(0, 0);
        Node* keepNode2 = gridMatrix->getNode(1, 1);
        Node* keepNode3 = gridMatrix->getNode(2, 2);

        gridMatrix->getNode(0, 1)->setVisited(true);
        gridMatrix->getNode(1, 0)->setVisited(true);
        gridMatrix->getNode(1, 2)->setVisited(true);

        QVector<Node*> nodesToKeep = {keepNode1, keepNode2, keepNode3};
        gridMatrix->clearVisitedExcept(nodesToKeep);

        REQUIRE(gridMatrix->getNode(0, 0)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(1, 1)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(2, 2)->isVisited() == false);

        REQUIRE(gridMatrix->getNode(0, 1)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(1, 0)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(1, 2)->isVisited() == false);
    }

    SECTION("GridMatrix::clearVisitedExcept: case when weight changes") {
        mazeSpec spec;
        int rows = 3;
        int cols = 3;
        spec.setRows(rows);
        spec.setCols(cols);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);

        Node* keepNode1 = gridMatrix->getNode(0, 0);
        Node* keepNode2 = gridMatrix->getNode(1, 1);
        Node* keepNode3 = gridMatrix->getNode(2, 2);

        gridMatrix->getNode(0, 1)->setVisited(true);

        gridMatrix->getNode(1, 0)->setVisited(true);
        gridMatrix->getNode(1,0)->setWeight(10);

        gridMatrix->getNode(1, 2)->setVisited(true);
        gridMatrix->getNode(1,2)->setWeight(5);

        QVector<Node*> nodesToKeep = {keepNode1, keepNode2, keepNode3};
        gridMatrix->clearVisitedExcept(nodesToKeep);

        REQUIRE(gridMatrix->getNode(0, 0)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(1, 1)->isVisited() == false);
        REQUIRE(gridMatrix->getNode(2, 2)->isVisited() == false);

        REQUIRE(gridMatrix->getNode(0, 1)->isVisited() == false);

        REQUIRE(gridMatrix->getNode(1, 0)->getWeight() == 10);
        REQUIRE(gridMatrix->getNode(1, 2)->getWeight() == 5);
    }

    SECTION("GridMatrix::saveMatrixToJson - ")
    {

        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* matrix = new GridMatrix(spec, nullptr);
        matrix->getNode(0,0)->setStartNode(true);
        matrix->getNode(2,2)->setEndNode(true);
        matrix->getNode(1,1)->setObstacle(true);

        QString filePath = "test_matrix.json";


        matrix->saveMatrixToJson(filePath);


        QFile file(filePath);
        REQUIRE(file.exists());

        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return;
        }

        QByteArray jsonData = file.readAll();
        file.close();

        QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonData);

        if (jsonDocument.isNull() || jsonDocument.isEmpty()) {
            return;
        }

        QJsonObject json = jsonDocument.object();

        REQUIRE(json["rows"] == 3);
        REQUIRE(json["columns"] == 3);

        REQUIRE(json["nodes"][0]["row"] == 0);
        REQUIRE(json["nodes"][0]["column"] == 0);
        REQUIRE(json["nodes"][0]["start"] == true);


        REQUIRE(json["nodes"][8]["row"].toInt() == 2);
        REQUIRE(json["nodes"][8]["column"].toInt() == 2);
        REQUIRE(json["nodes"][8]["end"] == true);

        REQUIRE(json["nodes"][4]["row"] == 1);
        REQUIRE(json["nodes"][4]["column"] == 1);
        REQUIRE(json["nodes"][4]["obstacle"] == true);


        file.remove();
        delete matrix;
    }



}
