#include <catch.hpp>

#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/Heuristic.h"

TEST_CASE("EuclideanDistance calculation", "[Heuristic]") {

    SECTION("EuclideanDistance - diferent nodes"){
    EuclideanDistance euclidean;

    Node node1(0, 0,1,1);
    Node node2(3, 4,1,1);

    double result = euclidean.calculate(node1, node2);

    double expected = std::sqrt(std::pow(3 - 0, 2) + std::pow(4 - 0, 2));

    REQUIRE(result == Approx(expected));
    }


    SECTION("EuclideanDistance - same node"){
    EuclideanDistance euclidean;

    Node node1(0, 0,1,1);
    Node node2(0, 0,1,1);

    double result = euclidean.calculate(node1, node2);

    double expected = std::sqrt(std::pow(0 - 0, 2) + std::pow(0 - 0, 2));

    REQUIRE(result == Approx(expected));
    }
}

TEST_CASE("ManhattanDistance calculation", "[Heuristic]") {
    SECTION("ManhattanDistance - diferent nodes") {
    ManhattanDistance manhattan;

    Node node1(0,0,1,1);
    Node node2(3,4,1,1);

    double result = manhattan.calculate(node1, node2);

    double expected = std::abs(3 - 0) + std::abs(4 - 0);

    REQUIRE(result == Approx(expected));
    }

    SECTION("ManhattanDistance - same node") {
    ManhattanDistance manhattan;

    Node node1(3,4,1,1);
    Node node2(3,4,1,1);

    double result = manhattan.calculate(node1, node2);

    double expected = std::abs(3 - 3) + std::abs(4 - 4);

    REQUIRE(result == Approx(expected));
    }
}
