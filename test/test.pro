TEMPLATE = app
QT       += core gui widgets

CONFIG += c++17

SOURCES += \
    bfs_test.cpp \
    dfs_test.cpp \
    dijkstra_test.cpp \
    heuristic_test.cpp \
    main.cpp \
    ../visuAlgo/Resources/Algorithm.cpp \
    ../visuAlgo/Resources/AStar.cpp \
    ../visuAlgo/Resources/BFS.cpp \
    ../visuAlgo/Resources/DFS.cpp \
    ../visuAlgo/Resources/Dijkstra.cpp \
    ../visuAlgo/Resources/GridMatrix.cpp \
    ../visuAlgo/Resources/Heuristic.cpp \
    ../visuAlgo/Resources/Node.cpp \
    ../visuAlgo/Resources/Walker.cpp \
    ../visuAlgo/Resources/mazespec.cpp \
    ../visuAlgo/Resources/widget.cpp \
    testAStar.cpp \
    testGridMatrix.cpp \
    node_test.cpp \
    walker_test.cpp

HEADERS += \
    catch.hpp \
    ../visuAlgo/Headers/AStar.h \
    ../visuAlgo/Headers/Algorithm.h \
    ../visuAlgo/Headers/BFS.h \
    ../visuAlgo/Headers/DFS.h \
    ../visuAlgo/Headers/Dijkstra.h \
    ../visuAlgo/Headers/GridMatrix.h \
    ../visuAlgo/Headers/Heuristic.h \
    ../visuAlgo/Headers/Node.h \
    ../visuAlgo/Headers/Walker.h \
    ../visuAlgo/Headers/mazespec.h \
    ../visuAlgo/Headers/widget.h

TARGET = Test

FORMS += \
    ../visuAlgo/Forms/mazespec.ui \
    ../visuAlgo/Forms/widget.ui
