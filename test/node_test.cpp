#include <catch.hpp>

#include <cmath>

#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/Heuristic.h"

TEST_CASE("class Node", "[class]")
{
    SECTION("after calling setStartNode method startNode should NOT be nullptr and attribute "
            "node->start should be true"){

        Node* node = new Node(1,1,1,1);

        node->setStartNode(true);

        REQUIRE(node->startNode != nullptr);
        REQUIRE(node->start == true);
    }

    SECTION("after calling setEndNode method endNode should NOT be nullptr and attribute "
            "node->end should be true"){

        Node *node = new Node(1,1,1,1);

        node->setEndNode(true);

        REQUIRE(node->endNode != nullptr);
        REQUIRE(node->end == true);
    }

    SECTION("method getNeighbors: case when neighbors list is empty") {
        Node *node = new Node(1,1,1,1);

        int result = node->getNeighbors().size();

        REQUIRE(result == 0);
    }


    SECTION("method getNeighbors returns list of all neighbors") {
        Node *node = new Node(1,1,1,1);

        QList<Node*> tmp_neighbors;

        for (int i = 0; i < 5; i++) {
            Node *neighbor = new Node(1,1,1,1);
            node->addNeighbor(neighbor);
            tmp_neighbors.append(neighbor);
        }


        REQUIRE(tmp_neighbors == node->getNeighbors());
    }

    SECTION("method addNeighbor: case when neighbor is nullptr then it shouldn't be added in neighbors"){
        Node *node = new Node(1,1,1,1);
        Node *neighbor = nullptr;

        node->addNeighbor(neighbor);

        QList<Node*> result = node->getNeighbors();

        REQUIRE(result.size() == 0);
    }


    SECTION("after calling  method addNeighbor neighbor should be at the end of the neighbors list"){

        Node *node = new Node(1,1,1,1);
        Node *neighbor = new Node(1,1,1,1);


        node->addNeighbor(neighbor);

        QList<Node*> result = node->getNeighbors();


        REQUIRE(result[0] == neighbor);
    }

    SECTION("method setVisited: after calling setVisited with true, node color should be updated and visited should be true") {
        Node* node = new Node(1, 1, 1, 1);

        node->setVisited(true);


        REQUIRE(node->isVisited() == true);

        QColor visitedColor = node->brush().color();
        REQUIRE(visitedColor == QColor(255, 165, 0));

        delete node;
    }

    SECTION("method setVisited: after calling setVisited with false, visited should be false and color should be updated") {

        Node* node= new Node(1, 1, 1, 1);
        node->setVisited(true);


        node->setVisited(false);


        REQUIRE(node->isVisited() == false);

        QColor notVisitedColor = node->brush().color();

        REQUIRE(notVisitedColor == QColor(106, 212, 157));

        delete node;
    }

    SECTION("method setWeight: when weight is greater than 0 and node is not an obstacle, start node, or end node") {
        Node* node = new Node(1, 1, 1, 1);


        node->setWeight(8);


        REQUIRE(node->getWeight() == 8);
        REQUIRE(node->brush().color() == QColor(0, 255 - 2 * 8, 0));

        delete node;
    }

    SECTION("method setWeight: when node is an obstacle") {

        Node* node = new Node(1, 1, 1, 1);


        node->setObstacle(true);
        node->setWeight(5);


        REQUIRE(node->getWeight() == 5);
        REQUIRE(node->brush().color() == QColor(Qt::black));

        delete node;
    }

    SECTION("method setWeight: when weight is 0") {

        Node* node = new Node(1, 1, 1, 1);


        node->setWeight(0);


        REQUIRE(node->getWeight() == 0);
        REQUIRE(node->brush().color() == QColor(106, 212, 157));

        delete node;
    }

    SECTION("metod setWeight: when node is the start node") {

        Node* node = new Node(1, 1, 1, 1);


        node->setStartNode(true);
        node->setWeight(1);


        REQUIRE(node->getWeight() == 1);
        REQUIRE(node->brush().color() == Qt::yellow);

        delete node;
    }

    SECTION("method setWeight: when node is the end node") {

        Node* node = new Node(1, 1, 1, 1);


        node->setEndNode(true);
        node->setWeight(4);


        REQUIRE(node->getWeight() == 4);
        REQUIRE(node->brush().color() == Qt::blue);

        delete node;
    }

    SECTION("method setObstacle: when obstacle is true and node is not start or end node") {

        Node* node = new Node(1, 1, 1, 1);


        node->setObstacle(true);


        REQUIRE(node->isObstacle() == true);
        REQUIRE(node->getWeight() == 0);
        REQUIRE(node->brush().color() == QColor(Qt::black));

        delete node;
    }

    SECTION("method setObstacle: when obstacle is false") {

        Node* node = new Node(1, 1, 1, 1);


        node->setObstacle(false);

        REQUIRE(node->isObstacle() == false);
        REQUIRE(node->brush().color() == QColor(106, 212, 157));

        delete node;
    }

    SECTION("method setObstacle: when node is start node") {

        Node* node = new Node(1, 1, 1, 1);
        node->setStartNode(true);


        node->setObstacle(true);


        REQUIRE(node->isObstacle() == false);
        REQUIRE(node->brush().color() == QColor(Qt::yellow));

        delete node;
    }

    SECTION("method setObstacle: when node is end node") {

        Node* node = new Node(1, 1, 1, 1);
        node->setEndNode(true);


        node->setObstacle(true);


        REQUIRE(node->isObstacle() == false); // Obstacle should not be set for end node
        REQUIRE(node->brush().color() == QColor(Qt::blue));

        delete node;
    }


    SECTION("method getFScore: getFScore returns correct value"){
        Node start(0,0,10,15);
        Node goal(5,5,10,15);

        start.setGScore(10);
        EuclideanDistance heuristic;

        double expectedFScore = start.getGScore() + heuristic.calculate(start, goal);
        double actualFScore = start.getFScore(&heuristic, &goal);

        REQUIRE(expectedFScore == actualFScore);
    }


    SECTION("method getDistanceToNeighbor: when neighbor is not null"){

        Node node1(0, 0, 1, 1);
        Node node2(3, 4, 1, 1);


        double result = node1.getDistanceToNeighbor(&node2);


        double expectedDistance = 5.0;
        REQUIRE(result == expectedDistance);
    }

    SECTION("method getDistanceToNeighbour: when neighbor is null"){

        Node node1(0, 0, 1, 1);
        const Node* nullNeighbor = nullptr;


        double result = node1.getDistanceToNeighbor(nullNeighbor);


        double defaultDistance = 0.0;
        REQUIRE(result == defaultDistance);
    }

    SECTION("method resetNode: resetting a node with various attributes set"){

        Node node(1, 1, 1, 1);
        node.setVisited(true);
        node.setObstacle(true);
        node.setStartNode(true);
        node.setEndNode(true);
        node.setWeight(5);


        node.resetNode();


        REQUIRE(node.isVisited() == false);
        REQUIRE(node.isObstacle() == false);
        REQUIRE(node.isStartNode() == false);
        REQUIRE(node.isEndNode() == false);
        REQUIRE(node.getWeight() == 0);
        REQUIRE(node.getParent() == nullptr);

        QColor initialColor(106, 212, 157);
        REQUIRE(node.brush().color() == initialColor);
    }

    SECTION("method resetNode: resetting a node that is already reset"){

        Node node(1, 1, 1, 1);

        node.resetNode();
        node.resetNode();


        REQUIRE(node.isVisited() == false);
        REQUIRE(node.isObstacle() == false);
        REQUIRE(node.isStartNode() == false);
        REQUIRE(node.isEndNode() == false);
        REQUIRE(node.getWeight() == 0);
        REQUIRE(node.getParent() == nullptr);

        QColor initialColor(106, 212, 157);
        REQUIRE(node.brush().color() == initialColor);
    }

    SECTION("method onNodeInPath - when the node is neither start nor end node") {

        Node node(1, 1, 1, 1);

        node.onNodeInPath();

        REQUIRE(node.brush().color() == QColor(Qt::red));
    }

    SECTION("method onNodeInPath -when the node is the end node") {
        Node node(1, 1, 1, 1);
        node.setEndNode(true);


        node.onNodeInPath();

        REQUIRE(node.brush().color() == QColor(Qt::blue));
    }

    SECTION("method onNodeInPath - when the node is the start node") {

        Node node(1, 1, 1, 1);
        node.setStartNode(true);

        node.onNodeInPath();

        REQUIRE(node.brush().color() == QColor(Qt::yellow));
    }

}
