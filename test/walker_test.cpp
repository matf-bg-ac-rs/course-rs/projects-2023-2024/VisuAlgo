#include <catch.hpp>
#include "../visuAlgo/Headers/Walker.h"
#include "../visuAlgo/Headers/Algorithm.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/Heuristic.h"
#include "../visuAlgo/Headers/Node.h"


TEST_CASE("Walker class", "[Walker]") {


    SECTION("method totalWeight: Empty path should have total weight of 0") {
        Walker* walker = new Walker(nullptr, nullptr, nullptr, 100);
        QVector<Node*> emptyPath;
        int result = walker->totalWeight(emptyPath);

        REQUIRE(result == 0);
    }

    SECTION("method totalWeight: for path with one node") {
        Walker* walker = new Walker(nullptr, nullptr, nullptr, 100);
        Node *node = new Node(1, 1, 1, 1);
        QVector<Node*> path;
        path.append(node);

        int result = walker->totalWeight(path);

        REQUIRE(result == node->getWeight());
    }

    SECTION("method totalWeight:t for path with multiple nodes") {
        Walker* walker = new Walker(nullptr, nullptr, nullptr, 100);
        Node* node1 = new Node(1, 1, 1, 1);
        Node* node2 = new Node(2, 2, 1, 1);
        Node* node3 = new Node(3, 3, 1, 1);
        QVector<Node*> path;
        path.append(node1);
        path.append(node2);
        path.append(node3);

        int result = walker->totalWeight(path);

        REQUIRE(result == (node1->getWeight() + node2->getWeight() + node3->getWeight()));
    }

    SECTION("method totalWeight: for path with zero weights") {
        Walker* walker = new Walker(nullptr, nullptr, nullptr, 100);
        Node* node1 = new Node(1, 1, 1, 1);
        Node* node2 = new Node(2, 2, 1, 1);
        Node* node3 = new Node(3, 3, 1, 1);

        node1->setWeight(0);
        node2->setWeight(0);
        node3->setWeight(0);

        QVector<Node*> path;
        path.append(node1);
        path.append(node2);
        path.append(node3);

        int result = walker->totalWeight(path);

        REQUIRE(result == 0);
    }

    SECTION("addNodeInPath method") {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker *walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* node1 = gridMatrix->getNode(1, 1);
        Node* node2 = gridMatrix->getNode(2, 2);
        Node* node3 = gridMatrix->getNode(3, 3);

        walker->addNodeInPath(node1);
        walker->addNodeInPath(node2);
        walker->addNodeInPath(node3);

        REQUIRE(walker->getPathNodes().size() == 3);
        REQUIRE(walker->getPathNodes().contains(node1));
        REQUIRE(walker->getPathNodes().contains(node2));
        REQUIRE(walker->getPathNodes().contains(node3));

    }

    SECTION("clearPath method") {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker *walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* node1 = gridMatrix->getNode(1, 1);
        Node* node2 = gridMatrix->getNode(2, 2);
        Node* node3 = gridMatrix->getNode(3, 3);

        walker->addNodeInPath(node1);
        walker->addNodeInPath(node2);
        walker->addNodeInPath(node3);

        walker->clearPath();

        REQUIRE(walker->getPathNodes().isEmpty());

    }

    SECTION("setVisitedWalker method") {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker *walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* node = gridMatrix->getNode(1, 1);

        walker->setVisitedWalker(node, true);

        REQUIRE(node->isVisited() == true);

        walker->setVisitedWalker(node, false);

        REQUIRE(node->isVisited() == false);
    }

    SECTION("getStartNode method") {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker *walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* startNode = gridMatrix->getNode(1, 1);
        startNode->setStartNode(true);

        REQUIRE(walker->getStartNode()->isStartNode());

        startNode->setStartNode(false);

        REQUIRE(walker->getStartNode() == nullptr);
    }

    SECTION("getEndNode method") {

        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker *walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* endNode = gridMatrix->getNode(5, 5);
        endNode->setEndNode(true);

        REQUIRE(walker->getEndNode()->isEndNode());

        endNode->setEndNode(false);

        REQUIRE(walker->getEndNode() == nullptr);
    }

    SECTION("setVisitedWalker method: visited status and brush color") {
        mazeSpec spec;
        spec.setRows(10);
        spec.setCols(10);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Walker* walker = new Walker(nullptr, gridMatrix, nullptr, 0, nullptr);

        Node* node = gridMatrix->getNode(1, 1);

        walker->setVisitedWalker(node, true);
        REQUIRE(node->isVisited() == true);
        REQUIRE(node->brush() == QColor(255, 165, 0));

        walker->setVisitedWalker(node, false);
        REQUIRE(node->isVisited() == false);
        REQUIRE(node->brush() == QColor(106, 212, 157));
    }

}
