#include <catch.hpp>

#include<list>
#include<utility>

#include "../visuAlgo/Headers/Node.h"
#include "../visuAlgo/Headers/GridMatrix.h"
#include "../visuAlgo/Headers/mazespec.h"
#include "../visuAlgo/Headers/Walker.h"
#include "../visuAlgo/Headers/Dijkstra.h"
#include "../visuAlgo/Headers/Heuristic.h"

using namespace std;

TEST_CASE("Dijkstra Algorithm") {
    SECTION("Case when path is found, without obstacles: For given begin and end node algorithm finds path,"
            " and total weight between them") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Node* start = gridMatrix->getNode(0,0);
        Node* end = gridMatrix->getNode(2,2);


        Node* node = gridMatrix->getNode(1,1);
        node->setWeight(1);


        start->setStartNode(true);
        end->setEndNode(true);


        Algorithm *algorithm = new Dijkstra();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm, gridMatrix, heuristic, 1);

        list<pair<int,int>> visited;
        visited.push_back(make_pair(0,0));
        visited.push_back(make_pair(0,1));
        visited.push_back(make_pair(1,0));
        visited.push_back(make_pair(0,2));
        visited.push_back(make_pair(1,2));
        visited.push_back(make_pair(2,0));
        visited.push_back(make_pair(2,1));
        visited.push_back(make_pair(2,2));
        visited.push_back(make_pair(1,1));



        walker->calculatePath();

        for(auto node : walker->visitedNodes){
            REQUIRE(node->getPosition().first == visited.front().first);
            REQUIRE(node->getPosition().second == visited.front().second);
            visited.pop_front();
        }

        list<pair<int,int>> realPath;
        realPath.push_back(make_pair(1,2));
        realPath.push_back(make_pair(0,1));


        for(auto node : walker->getPathNodes()) {
            REQUIRE(node->getPosition().first == realPath.front().first);
            REQUIRE(node->getPosition().second == realPath.front().second);
            realPath.pop_front();
        }

        QVector<Node*> path = walker->getPathNodes();
        REQUIRE(walker->totalWeight(path) == 0);


    }


    SECTION("Case when path is found, with obstacles: For given begin and end node algorithm finds path,"
            " and total weight between them") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Node* start = gridMatrix->getNode(0,0);
        Node* end = gridMatrix->getNode(2,2);


        Node* node1 = gridMatrix->getNode(1,1);
        node1->setWeight(1);

        Node* node2 = gridMatrix->getNode(1,0);
        node2->setObstacle(true);

        start->setStartNode(true);
        end->setEndNode(true);


        Algorithm *algorithm = new Dijkstra();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm, gridMatrix, heuristic, 1);

        list<pair<int,int>> visited;
        visited.push_back(make_pair(0,0));
        visited.push_back(make_pair(1,0));
        visited.push_back(make_pair(2,0));
        visited.push_back(make_pair(2,1));
        visited.push_back(make_pair(2,2));
        visited.push_back(make_pair(1,2));
        visited.push_back(make_pair(0,2));
        visited.push_back(make_pair(1,1));



        walker->calculatePath();

        for(auto node : walker->visitedNodes){
            REQUIRE(node->getPosition().first == visited.front().first);
            REQUIRE(node->getPosition().second == visited.front().second);
            visited.pop_front();
        }

        list<pair<int,int>> realPath;
        realPath.push_back(make_pair(2,1));
        realPath.push_back(make_pair(1,0));



        for(auto node : walker->getPathNodes()) {
            REQUIRE(node->getPosition().first == realPath.front().first);
            REQUIRE(node->getPosition().second == realPath.front().second);
            realPath.pop_front();
        }

        QVector<Node*> path = walker->getPathNodes();
        REQUIRE(walker->totalWeight(path) == 0);

    }



    SECTION("Case when path is found: For given begin and end node algorithm really finds shortest path,"
            " and total weight between them") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Node* start = gridMatrix->getNode(0,0);
        Node* end = gridMatrix->getNode(2,2);


        Node* node1 = gridMatrix->getNode(0,1);
        node1->setWeight(5);
        Node* node2 = gridMatrix->getNode(1,2);
        node2->setWeight(5);

        Node* node3 = gridMatrix->getNode(1,1);
        node3->setWeight(10);

        Node* node4 = gridMatrix->getNode(1,0);
        node4->setWeight(3);
        Node* node5 = gridMatrix->getNode(2,1);
        node5->setWeight(3);


        Node* node6 = gridMatrix->getNode(2,0);
        node6->setObstacle(true);


        start->setStartNode(true);
        end->setEndNode(true);


        Algorithm *algorithm = new Dijkstra();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm, gridMatrix, heuristic, 1);

        list<pair<int,int>> visited;
        visited.push_back(make_pair(0,0));
        visited.push_back(make_pair(0,1));
        visited.push_back(make_pair(1,0));
        visited.push_back(make_pair(2,0));
        visited.push_back(make_pair(1,2));
        visited.push_back(make_pair(2,2));
        visited.push_back(make_pair(1,1));
        visited.push_back(make_pair(2,1));



        walker->calculatePath();

        for(auto node : walker->visitedNodes){
            REQUIRE(node->getPosition().first == visited.front().first);
            REQUIRE(node->getPosition().second == visited.front().second);
            visited.pop_front();
        }



        list<pair<int,int>> realPath;
        realPath.push_back(make_pair(1,2));
        realPath.push_back(make_pair(0,1));


        for(auto node : walker->getPathNodes()) {
            REQUIRE(node->getPosition().first == realPath.front().first);
            REQUIRE(node->getPosition().second == realPath.front().second);
            realPath.pop_front();
        }

        QVector<Node*> path = walker->getPathNodes();
        REQUIRE(walker->totalWeight(path) == 6);


    }

    SECTION("Case when path is not found: For given begin and end node algorithm doesn't find path,"
            " and total weight between them") {
        mazeSpec spec;
        spec.setRows(3);
        spec.setCols(3);

        GridMatrix* gridMatrix = new GridMatrix(spec, nullptr);
        Node* start = gridMatrix->getNode(0,0);
        Node* end = gridMatrix->getNode(2,2);


        Node* node1 = gridMatrix->getNode(2,1);
        node1->setObstacle(true);
        Node* node2 = gridMatrix->getNode(1,1);
        node2->setObstacle(true);
        Node* node3 = gridMatrix->getNode(1,2);
        node3->setObstacle(true);


        start->setStartNode(true);
        end->setEndNode(true);


        Algorithm *algorithm = new Dijkstra();
        Heuristic *heuristic = new EuclideanDistance();
        Walker* walker = new Walker(algorithm, gridMatrix, heuristic, 1);

        list<pair<int,int>> visited;
        visited.push_back(make_pair(0,0));
        visited.push_back(make_pair(0,1));
        visited.push_back(make_pair(1,0));
        visited.push_back(make_pair(0,2));
        visited.push_back(make_pair(2,0));


        walker->calculatePath();

        for(auto node : walker->visitedNodes){
            REQUIRE(node->getPosition().first == visited.front().first);
            REQUIRE(node->getPosition().second == visited.front().second);
            visited.pop_front();
        }

        QVector<Node*> path = walker->getPathNodes();
        REQUIRE(path.size() == 0);

        REQUIRE(walker->totalWeight(path) == 0);


    }


}
