# VisuAlgo

## Opis igre
Ova aplikacija ima za cilj da vizualno prikaže rad algoritama. Ideja je da se krećemo kroz lavirint(matricu) od početnog do krajnjeg čvora, pri tom zaobilazeći sve prepreke koje nam se nadju na putu. Svrha aplikacije je da na drugačiji način pomogne u lakšem razumevanju funkcionisanja algoritama.

## Programski jezik i okruženje   
Language: C++  
Framework: Qt6  
IDE: Qt Creator  

## Instalacija Qt Creator-a


- QtCreator možete pronaći i preuzeti na ovoj veb stranici: https://www.qt.io/download
- Nakon otvaranja linka, prilikom spuštanja na kraj stranice nalazi se opcija za preuzimanje za otvorenu source upotrebu i kliknite na "go open source".
- Kada završite prethodni korak, spustite se do kraja stranice i kliknite na "Download the Qt online Installer".
- Zatim, kliknite na "Download" i preuzeli ste QtInstaller.
- Sada, jedino što trebate učiniti je preuzeti željenu verziju Qt-a.

## Preuzimanje VisuAlgo projekta
Da biste preuzeli naš projekat VisuAlgo, potrebno je da otvorite terminal i ukucate: git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/VisuAlgo.git. Kada izvršite ovu komandu, projekat će biti kloniran na Vaš uređaj.  

## Start with Qt
- Nakon preuzimanja QtCreator-a, otvorite aplikaciju na svom računaru.
- Kada otvorite aplikaciju, kliknite na "Open project".
- Sada pronađite folder u kojem je projekat preuzet i otvorite datoteku visuAlgo.pro.
- Da biste uradili build projekta, kliknite na ikonu "Build" koja se nalazi u donjem levom uglu ekrana.
- Kada je odradjen build projekta, pokrenite ga klikom na "Run" koji se takodje nalazi u donjem levom uglu ekrana.
- Projekat sada radi i možete ga koristiti.

## Demo snimak
https://youtu.be/jAE3pDFdosM
## Članovi
 - <a href="https://gitlab.com/TOTOOO">Vladan Markov 298/2019</a>
 - <a href="https://gitlab.com/sakisa">Sandra Petrovic 84/2019</a>
 - <a href="https://gitlab.com/IvanaNest">Ivana Nestorovic 130/2019</a>
 - <a href="https://gitlab.com/AndrijanaIvkovic">Andrijana Ivkovic 115/2019</a>
 - <a href="https://gitlab.com/LazarR33">Lazar Rankovic 107/2019</a>
 - <a href="https://gitlab.com/ivadjordjevic">Iva Djordjevic 267/2019</a>
